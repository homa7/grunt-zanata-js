/*
 * grunt-zanata-js
 * https://bitbucket.org/tagoh/grunt-zanata-js
 *
 * Copyright (c) 2016 Akira TAGOH
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: ['Gruntfile.js', 'tasks/*.js'],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Before generating any new files, remove any previously-created files.
    clean: {
      tests: ['tmp']
    },

    // Configuration to be run (and then tested).
    zanata: {
      pull_sample: {
        // options is simply passed to ZanataClient.Config
        // of course if you have $XDG_CONFIG_HOME/zanata.ini
        // it will be used as a base config.
        options: {
          url: 'https://translate.stage.zanata.org',
          project: 'grunt-zanata-js',
          'project-version': 'master',
          'project-type': 'gettext',
        },
        files: [
          {src: 'test/fixtures/test.pot'},
//          {src: 'test/fixtures/test.po'}, // not supported
        ]
      },
      pull_dir_sample: {
        options: {
          url: 'https://translate.stage.zanata.org',
          project: 'grunt-zanata-js',
          'project-version': 'master',
          'project-type': 'gettext',
          skeletons: true
        },
        files: [
          {src: 'test/fixtures', type: 'source'}, // download only POT files
          {src: 'test/fixtures'}, // download both POT and PO files
          {src: 'test/fixtures', type: 'trans'} // download only PO files
        ]
      },
      push_dir_sample: {
        options: {
          url: 'https://translate.stage.zanata.org',
          project: 'grunt-zanata-js',
          'project-version': 'master',
          'project-type': 'gettext',
          skeletons: true
        },
        files: [
          {src: 'test/fixtures', type: 'source'}, // upload only POT files
          {src: 'test/fixtures/po', type: 'trans'},  // upload only PO files
        ]
      },
      pull_dir_docid: {
        options: {
          url: 'https://translate.stage.zanata.org',
          project: 'grunt-zanata-js',
          'project-version': 'master',
          'project-type': 'gettext',
          skeletons: true
        },
        files: [
          {src: 'test/fixtures', type: 'source', docId: 'test'}, // download only POT files
          {src: 'test/fixtures/po', type: 'trans', docId: 'test'} // download only PO files
        ]
      }
    }
  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-readme');

  grunt.registerTask('test', ['clean', 'zanata']);
  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'repos', 'readme', 'test']);
};
