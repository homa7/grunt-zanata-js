# grunt-zanata-js [![NPM version](https://badge.fury.io/js/grunt-zanata-js.png)](http://badge.fury.io/js/grunt-zanata-js)

> A Grunt task to perform operations from/to Zanata through zanata-js

## Getting Started
_If you haven't used [grunt][] before, be sure to check out the [Getting Started][] guide._

From the same directory as your project's [Gruntfile][Getting Started] and [package.json][], install this plugin with the following command:

```bash
npm install grunt-zanata-js --save-dev
```

Once that's done, add this line to your project's Gruntfile:

```js
grunt.loadNpmTasks('grunt-zanata-js');
```

If the plugin has been installed correctly, running `grunt --help` at the command line should list the newly-installed plugin's task or tasks. In addition, the plugin should be listed in package.json as a `devDependency`, which ensures that it will be installed whenever the `npm install` command is run.

[grunt]: http://gruntjs.com/
[Getting Started]: https://github.com/gruntjs/grunt/blob/devel/docs/getting_started.md
[package.json]: https://npmjs.org/doc/json.html

## The "zanata" task

### Overview
In your project's Gruntfile, add a section named `zanata` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  zanata: {
    // To push POT/PO files
    push_unique_name: {
      options: {
        // Task-specific options go here.
      },
      files: [
        ...
      ]
    },
    pull_unique_name: {
      ...
    }
  },
})
```

### Options

#### options.url
Type: `String`

Zanata server URL to connect. if not specified, a value in zanata.xml at the current directory will be used.

#### options.project
Type: `String`

Project name that hosted at Zanata server. if not specified, a value in zanata.xml at the current directory will be used.

#### options['project-version']
Type: `String`

Project version in the project at Zanata server. sources/translations will be pushed/pulled against this version.
if not specified, a value in zanata.xml at the current directory will be used.

#### options['project-type']
Type: `String`

Project type. that should be one of `gettext`, `podir`, `properties`, `utf8properties`, `xliff`, `xml`, or `file`.
if not specified, a value in zanata.xml at the current directory will be used.

#### options.skeletons
Type: `Boolean`
Default value: `false`

A boolean value to determine if creating skeleton entries for PO files which have not been translated yet.

#### options.verbose
Type: `Boolean`
Default value: `false`

A boolean value to display more detailed messages.

### Usage Examples

#### Push/Pull files under the certain directory
In this example, the targeted files are all of files at the specified directory. what kind of files will be performed is able to manage with `type` property. `'source'` will download/upload POT files only, `'trans'` will download/upload PO files only, and `'both'` or no `type` property will do both.
and if you want to specify locales rather than what locales are available on Zanata server, you can use `locales` property instead.

```js
grunt.initConfig({
  zanata: {
    push_dir: {
      options: {
        ...
      },
      files: [
        {src: 'pot', type: 'source'}, // upload POT files only
	{src: 'po', type: 'trans'},   // upload PO files only
      ]
    },
    pull_dir: {
      options: {
        ...
      },
      files: [
        {src: 'pot', type: 'source'}, // download POT files only
	{src: 'po', type: 'trans'},   // download PO files only
      ]
    }
  }
})
```

#### Push/Pull certain files
In this example, the targeted source file are only one file in files property. this format doesn't support PO files' operations. please make sure if the document is available at the Zanata server first.

```js
grunt.initConfig({
  zanata: {
    push_file: {
      options: {
        ...
      },
      files: [
        {src: 'pot/test.pot'},
      ]
    },
    pull_file: {
      options: {
        ...
      },
      files: [
        {src: 'pot/test.pot'},
      ]
    }
  }
})
```

## License
Copyright (c) 2016 Akira TAGOH
Licensed under the [MIT license](LICENSE-MIT).

***

Project created by [Akira TAGOH](https://bitbucket.org/tagoh).

_This file was generated on Wed May 11 2016 12:23:07._
